// import React from "react";
import loadingBackground from "./assets/img/space-background1.jpg";
import { Container, Grid, Typography } from "@mui/material";

function App() {
  return (
    <div
      style={{
        backgroundColor: "#f6f6f6",
        display: "flex",
        justifyContent: "center",
        minHeight: "100vh",
        width: "100vw",
      }}
    >
      <div
        style={{
          backgroundImage: `url(${loadingBackground})`,
          height: "680px",
          width: "580px",
          backgroundPosition: "top",
          backgroundSize: "cover",
          color: "white",
          fontFamily: '"Courier New", Courier, monospace',
          fontSize: "24px",
        }}
      >
        <Container maxWidth='lg'>
          <Grid container spacing={1} height='100%' pt={85}>
            <Grid xs={12}>
              <Typography
                variant='h1'
                color='black'
                textAlign={"center"}
                justifyContent='flex-end'
              >
                Novus Net
              </Typography>
            </Grid>
            <Grid xs={12}>
              <Typography variant='h4' color='black' textAlign={"center"}>
                Muy pronto podrás conocer más acerca de nuestros servicios
              </Typography>
            </Grid>
          </Grid>
        </Container>
      </div>
    </div>
  );
}

export default App;
